package net.astoriamc.ubercore.global;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class async {
    public static void statementExecuteAsyncQuery(String s, Connection connection){
        Thread thread = new Thread(new exq(s, connection));
        thread.start();
    }

    public static void statementExecuteAsyncUpdate(String s, Connection connection){
        Thread thread = new Thread(new exup(s, connection));
        thread.start();
    }
    public static void sendMessage(String s, String ip, int port){
        Thread thread = new Thread(new sendMsg(s, ip, port));
        thread.start();
    }

    private static class exup implements Runnable{
        private Connection connection;
        private String string;
        private exup(String s, Connection connection){
            this.connection = connection;
            string = s;
        }
        @Override
        public void run() {
            try {
                connection.createStatement().executeUpdate(string);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    private static class exq implements Runnable{
        private Connection connection;
        private String string;
        private exq(String s, Connection connection){
            this.connection = connection;
            string = s;
        }
        @Override
        public void run() {
            try {
                connection.createStatement().executeQuery(string);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    private static class sendMsg implements Runnable{
        private int port;
        private String string;
        private String ip;
        private sendMsg(String s, String ip, int port){
            this.port = port;
            string = s;
            this.ip = ip;
        }
        @Override
        public void run() {
            Client client = new Client();
            try {
                client.startConnection(ip,port);
                client.sendMessage(string);
                client.sendMessage("ShutdownSession");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
