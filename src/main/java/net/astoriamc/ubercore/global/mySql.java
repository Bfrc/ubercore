package net.astoriamc.ubercore.global;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class mySql {
    private Connection connection;

    public Connection openConnection(String host, int port, String database, String user, String password) {


        try {

            synchronized (this) {
                if (connection != null && !connection.isClosed()) {
                    return null;
                }

                Class.forName("com.mysql.jdbc.Driver");
                connection = (DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password));

            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
