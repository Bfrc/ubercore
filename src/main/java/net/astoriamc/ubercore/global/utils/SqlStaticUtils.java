package net.astoriamc.ubercore.global.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class SqlStaticUtils {
    public static int getUUPHbyMinecraftUUID(String uuid, Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        int puuph = -1;
        try {
            String sql = "SELECT * FROM UUPH where UUIDMCHASH = " + uuid.hashCode() + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next())
                puuph = rs.getInt("UUPH");
        } catch (SQLException throwables) {
            puuph = -1;
        }
        return puuph;
    }

    public static UUID getUUIDfromUUPH(int UUPH, Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        UUID UUID1 = null;
        try {
            String sql = "SELECT * FROM UUPH where UUPH = " + UUPH + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next())
                UUID1 = UUID.fromString(rs.getString("UUIDMC"));
        } catch (SQLException throwables) {
            UUID1 = null;
        }
        return UUID1;
    }

    public static int getUUPHbyMinecraftUUID(UUID uuid, Connection connection) throws SQLException {
        return getUUPHbyMinecraftUUID(uuid.toString(), connection);
    }

    public static int generateUUPH(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        int uuid = UUID.randomUUID().toString().hashCode();
        try {
            String sql = "SELECT * FROM UUPH where UUPH = " + uuid + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                long a = rs.getLong("UUPH");
                if (a != 0L)
                    return uuid;
            }
        } catch (SQLException throwables) {
            return generateUUPH(connection);
        }
        return uuid;
    }
}
