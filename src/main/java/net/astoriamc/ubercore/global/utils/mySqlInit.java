package net.astoriamc.ubercore.global.utils;

import net.astoriamc.ubercore.global.mySql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class mySqlInit {
    public Connection init(String host, int port, String database, String user, String password) throws SQLException {
        mySql mySql = new mySql();
        Connection connection = mySql.openConnection(host, port, database, user, password);
        Statement statement = connection.createStatement();
        System.out.println("[SQL] Connecting...");
        try {
            statement.execute("CREATE TABLE IF NOT EXISTS PlayerDataMc(\n" +
                    "    UUPH        int,\n" +
                    "    PlayerName        LONGTEXT,\n" +
                    "    JOINDATE        LONGTEXT,\n" +
                    "    LASTJOIN        LONGTEXT,\n" +
                    "    LEAVEDATE        LONGTEXT\n);");
            statement.execute("CREATE TABLE IF NOT EXISTS GlobalData(\n" +
                    "    UUPH        int,\n" +
                    "    UberCoins        int\n" +
                    ");");
            statement.execute("CREATE TABLE IF NOT EXISTS UUPH(\n" +
                    "    UUIDMC        LONGTEXT,\n" +
                    "    DCID        LONGTEXT,\n" +
                    "    DCNAME        LONGTEXT,\n" +
                    "    RANG        LONGTEXT,\n" +
                    "    UUIDMCHASH        int,\n" +
                    "    UUIDDCHASH        int,\n" +
                    "    UUPH        int\n" +
                    ");");
            System.out.println("[SQL]connected");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
