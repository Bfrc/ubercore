package net.astoriamc.ubercore.spigot.commands.ashop;

import me.mattstudios.mfgui.gui.components.util.ItemBuilder;
import me.mattstudios.mfgui.gui.guis.Gui;
import me.mattstudios.mfgui.gui.guis.GuiItem;
import net.astoriamc.ubercore.spigot.Ubercore;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ashop implements CommandExecutor {

    private Ubercore plugin = Ubercore.getPlugin(Ubercore.class);
    private int bal;
    private boolean conf;


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Connection connection = plugin.getConnection();

        Player player = (Player) sender;
        if (!(sender instanceof Player)) {
            System.out.println("You can`t do that in console");
            return true;
        }
        Statement statement = null;
        try {
            statement = plugin.getConnection().createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        int hashmc = player.getUniqueId().toString().hashCode();
        int puuph = 0;
        try {
            String sql = "SELECT * FROM UUPH Where UUIDMCHASH = " + hashmc + " ;";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                puuph = rs.getInt("UUPH");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        try {
            ResultSet result = statement.executeQuery("SELECT * FROM GlobalData WHERE UUPH = " + puuph + " ;");
            while (result.next()) {
                bal = result.getInt("UberCoins");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Gui gui = new Gui(1, "Astoria Shop");
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.translateAlternateColorCodes('&', "&cDuckiest sword of them all"));
        ItemStack a = itemStackGenerator(new ItemStack(Material.DIAMOND), true, "&cTHE DUCK DIAMOND", lore);
        GuiItem guiItem = new GuiItem(a, event -> {
            event.getWhoClicked().sendMessage("Hi");
            event.setCancelled(true);
        });
        lore.clear();
        a = itemStackGenerator(new ItemStack(Material.EMERALD_BLOCK), true, "&cBalance: " + bal, lore);
        GuiItem balance = new GuiItem(a, event -> {
            event.getWhoClicked().sendMessage("Your balance is: " + bal);
            event.setCancelled(true);
        });
        GuiItem placeholder = ItemBuilder.from(Material.GRAY_STAINED_GLASS_PANE).asGuiItem();

        lore.clear();
        lore.add("The Diamond Sword");
        a = itemStackGenerator(new ItemStack(Material.DIAMOND_SWORD), true, "&cDiamond Sword DLC Price: 30 AstoriaCoins", lore);
        GuiItem diamondswordDLC = new GuiItem(a, event -> {
            event.setCancelled(true);
            try {
                itemConformation(itemStackGenerator(new ItemStack(Material.DIAMOND_SWORD), true, "&cDiamond Sword DLC Price: 30 AstoriaCoins", lore), event.getWhoClicked(), "afterbuySword", 28);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

        });


        gui.addItem(placeholder);
        gui.addItem(guiItem);
        gui.addItem(placeholder);
        gui.addItem(diamondswordDLC);
        gui.addItem(placeholder);
        gui.addItem(placeholder);
        gui.setItem(8, balance);
        gui.open(player);
        return true;
    }

    public ItemStack itemStackGenerator(ItemStack itemStack, boolean enchanted, String name, List lore) {

        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        if (enchanted == true) {
            itemStack.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        }
        return itemStack;
    }

    public void itemConformation(ItemStack itemStack, HumanEntity player, String methodname, int price) throws NoSuchMethodException {


        Statement statement = null;
        try {
            statement = plugin.getConnection().createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        int hashmc = player.getUniqueId().toString().hashCode();
        int puuph = 0;
        try {
            String sql = "SELECT * FROM UUPH Where UUIDMCHASH = " + hashmc + " ;";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                puuph = rs.getInt("UUPH");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        List<String> lore = new ArrayList<String>();
        final Method method = guiAfter.class.getDeclaredMethod(methodname, Boolean.class, Player.class);
        if (price == 0) {
            try {
                method.invoke(null, true, player);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return;
        }
        try {
            ResultSet result = statement.executeQuery("SELECT * FROM GlobalData WHERE UUPH = " + puuph + ";");
            while (result.next()) {
                bal = result.getInt("UberCoins");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (bal < price) {
            //player.sendMessage(chat.chat("&cYou don`t have enough money"));
            return;
        }


        Gui guic = new Gui(5, "BuyConformation");
        GuiItem placeholder = new GuiItem(itemStackGenerator(new ItemStack(Material.GRAY_STAINED_GLASS_PANE), false, " ", lore), event -> {
            event.setCancelled(true);
        });
        List<Integer> integers = new ArrayList<Integer>();
        for (int i = 0; i != 45; i++) {
            integers.add(i);
        }
        guic.setItem(integers, placeholder);
        GuiItem item4sale = new GuiItem(itemStack, event -> {
            event.setCancelled(true);
        });
        guic.setItem(2, 5, item4sale);


        lore.add(ChatColor.translateAlternateColorCodes('&', "&aConform"));
        ItemStack a = itemStackGenerator(new ItemStack(Material.GREEN_CONCRETE), true, "&aConform", lore);
        Statement finalStatement = statement;
        GuiItem Conform = new GuiItem(a, event -> {
            int hashmc1 = event.getWhoClicked().getUniqueId().toString().hashCode();
            int puuph1 = 0;
            try {
                String sql = "SELECT * FROM UUPH Where UUIDMCHASH = " + hashmc1 + " ;";
                ResultSet rs = finalStatement.executeQuery(sql);
                while (rs.next()) {
                    puuph1 = rs.getInt("UUPH");
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            try {
                ResultSet result = finalStatement.executeQuery("SELECT * FROM GlobalData WHERE UUPH = " + puuph1 + " ;");
                while (result.next()) {
                    if (result.getString("UUID").equals(player.getUniqueId().toString())) {
                        bal = result.getInt("UberCoins");

                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            event.setCancelled(true);
            int newbal = bal - price;
            try {
                finalStatement.executeUpdate("UPDATE GlobalData SET UberCoins = " + newbal + " WHERE UUPH = " + puuph1 + " ;");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            guic.close(event.getWhoClicked());
            event.getWhoClicked().sendMessage();
            try {
                method.invoke(null, true, event.getWhoClicked());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });


        lore.clear();
        lore.add(ChatColor.translateAlternateColorCodes('&', "&cDeny"));
        a = itemStackGenerator(new ItemStack(Material.RED_CONCRETE), true, "&cDeny", lore);
        GuiItem Deny = new GuiItem(a, event -> {
            event.setCancelled(true);
            guic.close(event.getWhoClicked());
            try {
                method.invoke(null, false, event.getWhoClicked());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });


        guic.setItem(4, 7, Deny);
        guic.setItem(4, 3, Conform);
        guic.open(player);
    }


}
