package net.astoriamc.ubercore.spigot;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.astoriamc.ubercore.global.utils.mySqlInit;
import net.astoriamc.ubercore.spigot.listener.onMessageSent;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import javax.security.auth.login.Configuration;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public final class Ubercore extends JavaPlugin implements PluginMessageListener {
    private Connection connection;
    public HashMap<UUID,String> hashMap = new HashMap<>();
    public void onEnable() {
        mySqlInit initSql = new mySqlInit();
        try {
            this.connection = initSql.init(this.getConfig().getString("Address"),
                    this.getConfig().getInt("Port"),
                    this.getConfig().getString("Database"),
                    this.getConfig().getString("User"),
                    this.getConfig().getString("Password"));
        } catch (SQLException throwables) {
            getLogger().info("mySql not working/has wrong credentials.");
        }
        System.out.println(connection);
        //getCommand("ashop").setExecutor( new ashop());
        new onMessageSent(this);
        checkIfBungee();
        if (!getServer().getPluginManager().isPluginEnabled( this))
            return;
        getServer().getMessenger().registerIncomingPluginChannel( this, "as:ubercore", this);
        getLogger().info("╔═╦════════════════════╦═╗");
        getLogger().info("║D║   AstoriaUberCore  ║C║");
        getLogger().info("║U║ An exclusive plugin║O║");
        getLogger().info("║C║  for astoriamc.net ║R║");
        getLogger().info("║K╠════════════════════╣E║");
        getLogger().info("╠═╝    Made by Bfrc    ╚═╣");
        getLogger().info("╚════════════════════════╝");
    }

    public void onDisable() {
        saveConfig();
    }

    private static String getPlayerPrefix(Player player) {
        LuckPerms luckPerms = LuckPermsProvider.get();
        User user = luckPerms.getUserManager().getUser(player.getUniqueId());
        return user.getCachedData().getMetaData().getPrefix();
    }

    private void checkIfBungee() {
        if (!getServer().getVersion().contains("Spigot") && !getServer().getVersion().contains("Paper") && !getServer().getVersion().contains("Tuinity")) {
            getLogger().severe("You probably run CraftBukkit... Please update atleast to spigot for this to work...");
            getLogger().severe("Plugin disabled!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if ((Objects.requireNonNull(getServer().spigot().getConfig().getConfigurationSection("settings"))).getBoolean("settings.bungeecord")) {
            getLogger().severe("This server is not BungeeCord.");
            getLogger().severe("If the server is already hooked to BungeeCord, please enable it into your spigot.yml aswell.");
            getLogger().severe("Plugin disabled!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    public void loadConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public int getBotPort() {
        int port = getConfig().getInt("discord-bot-port");
        return port;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (!channel.equalsIgnoreCase("as:ubercore")){ return;}
        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
        String data = in.readUTF();
        System.out.println(data);
        JsonElement jsonElement = (new JsonParser()).parse(data);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String subChannel = jsonObject.get("Server").getAsString();
        if (subChannel.equalsIgnoreCase(getConfig().getString("server-name"))) {
            System.out.println("Received transmission for server:" + data);
            Player player1;
            String msg;
            switch (jsonObject.get("typeSpigot").getAsString()) {
                case "DisMes":
                    player1 = null;
                    msg = jsonObject.get("Msg").getAsString();
                    try {
                        player1 = getServer().getPlayer(SqlStaticUtils.getUUIDfromUUPH(jsonObject.get("UUPH").getAsInt(), getConnection()));
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    if (player1.isOnline()) {
                        player1.chat(msg);
                        hashMap.put(player1.getUniqueId(), msg);
                    } else {
                        String preparedMessage = getPlayerPrefix(player) + player.getDisplayName() + ": " + msg;
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.sendMessage(preparedMessage);
                        }
                    }
                    break;

            }
        }
    }
}
