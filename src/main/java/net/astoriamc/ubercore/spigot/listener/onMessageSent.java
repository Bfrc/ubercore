package net.astoriamc.ubercore.spigot.listener;

import net.astoriamc.ubercore.global.Client;
import net.astoriamc.ubercore.global.async;
import net.astoriamc.ubercore.spigot.Ubercore;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;
import org.json.JSONObject;

import java.io.IOException;

public class onMessageSent implements Listener {
    private Ubercore plugin;

    public onMessageSent(Ubercore plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this,  plugin);
    }

    @EventHandler
    public void onMessageSent(AsyncPlayerChatEvent e) {
        if (plugin.getConfig().getBoolean("discord-live-chat")) {
            if (plugin.hashMap.get(e.getPlayer().getUniqueId()) == e.getMessage()){
                plugin.hashMap.remove(e.getPlayer().getUniqueId());
                return;
            }
            LuckPerms api = LuckPermsProvider.get();
            Player player = e.getPlayer();
            String group = api.getUserManager().getUser(player.getName()).getPrimaryGroup();
            JSONObject jo = new JSONObject();
            jo.put("user", player.getName());
            jo.put("msg", e.getMessage());
            jo.put("channel", plugin.getConfig().getString("discord-live-chat-chanel-id"));
            jo.put("rank", group);
            jo.put("type", "msg");
            async.sendMessage(jo.toString(), plugin.getConfig().getString("Address"), 6000);
        }
    }
}
