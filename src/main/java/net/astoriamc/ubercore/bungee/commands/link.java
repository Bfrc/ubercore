package net.astoriamc.ubercore.bungee.commands;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.Client;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLException;

public class link extends Command {
    private BungeeCore plugin;

    public link(BungeeCore plugin) {
        super("link");
        this.plugin = plugin;
        plugin.getProxy().getPluginManager().registerCommand( plugin, this);
    }

    public void execute(CommandSender sender, String[] args) {
        String id = args[0];
        if (sender instanceof ProxiedPlayer) {
            LuckPerms api = LuckPermsProvider.get();
            ProxiedPlayer player = (ProxiedPlayer) sender;
            String group = api.getUserManager().getUser(player.toString()).getPrimaryGroup();
            player.sendMessage("Account linked");
            Client client = new Client();
            JSONObject jo = new JSONObject();
            jo.put("nameMc", player.getName());
            jo.put("id", id);
            jo.put("rank", group);
            jo.put("type", "acceptLink");

            try {
                jo.put("UUPH", SqlStaticUtils.getUUPHbyMinecraftUUID(player.getUniqueId(), this.plugin.getConnection()));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                client.startConnection("127.0.0.1", 6000);
                client.sendMessage(jo.toString());
                client.sendMessage("ShutdownSession");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
