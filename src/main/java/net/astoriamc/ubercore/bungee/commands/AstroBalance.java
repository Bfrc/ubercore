package net.astoriamc.ubercore.bungee.commands;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AstroBalance extends Command {
    private final BungeeCore BungeeCore;

    public AstroBalance(BungeeCore bungeeCore) {
        super("abal");
        this.BungeeCore = bungeeCore;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage("You can`t do it in console");
            return;
        }
        Statement statement = null;
        try {
            statement = BungeeCore.getConnection().createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        long puuph = 0;
        try {
            puuph = SqlStaticUtils.getUUPHbyMinecraftUUID(player.getUniqueId(), BungeeCore.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            ResultSet result = statement.executeQuery("SELECT * FROM GlobalData WHERE UUPH = " + puuph + " ;");
            while (result.next()) {
                int bal = result.getInt("UberCoins");
                player.sendMessage("Your balance is: " + bal);
            }
        } catch (SQLException throwables) {
            //player.sendMessage(chat.chat("&cThe player dos not exist"));
        }

    }
}
