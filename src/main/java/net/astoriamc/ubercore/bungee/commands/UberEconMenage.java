package net.astoriamc.ubercore.bungee.commands;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UberEconMenage extends Command {

    private int balold;
    private Statement stmt;
    private BungeeCore BungeeCore;

    public UberEconMenage(BungeeCore bungeeCore) {
        super("aem");
        this.BungeeCore = bungeeCore;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        System.out.println(args[0]);
        System.out.println(args[1]);
        System.out.println(args[2]);
        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
        String sql;
        Connection conn = BungeeCore.getConnection();
        try {
            stmt = BungeeCore.getConnection().createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        long puuph = 0;
        try {
            puuph = SqlStaticUtils.getUUPHbyMinecraftUUID(player.getUniqueId(), conn);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            sql = "SELECT UberCoins FROM GlobalData Where UUPH =  " + puuph + ";";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                balold = rs.getInt("UberCoins");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        int newWalue;
        if (sender instanceof ProxiedPlayer) {
            if (!(player.hasPermission("UberCore.balance.menage"))) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou don`t have permission to do that"));
                return;
            } else {
                if (!(args.length >= 1 && args.length < 4)) {
                    player.sendMessage("usage: /AstroEconMenage <player> set|take|give|clear <amount>");
                    return;
                } else {
                    switch (args[1]) {
                        case "set":
                            newWalue = Integer.parseInt(args[2]);
                            break;
                        case "clear":
                            newWalue = 0;
                            break;
                        case "give":
                            newWalue = balold + Integer.parseInt(args[2]);
                            break;
                        case "remove":
                            newWalue = balold - Integer.parseInt(args[2]);
                            break;
                        default:
                            sender.sendMessage("usage: /AstroEconMenage <player> set|take|give|clear <amount>");
                            return;
                    }
                    System.out.println(newWalue);
                }
            }
        } else if (args.length > 1 && args.length < 4) {
            System.out.println("usage: /AstroEconMenage <player> set|take|give|clear <amount>");
            return;
        } else {
            switch (args[1]) {
                case "set":
                    newWalue = Integer.parseInt(args[2]);
                    break;
                case "clear":
                    newWalue = 0;
                    break;
                case "give":
                    newWalue = balold + Integer.parseInt(args[2]);
                    break;
                case "remove":
                    newWalue = balold - Integer.parseInt(args[2]);
                    break;
                default:
                    sender.sendMessage("usage: /AstroEconMenage <player> set|take|give|clear <amount>");
                    return;
            }
            System.out.println(newWalue);
        }


        try {
            sql = "UPDATE GlobalData SET UberCoins = " + newWalue + " WHERE UUPH = " + puuph + ";";
            stmt.executeUpdate(sql);

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
        }
    }
}