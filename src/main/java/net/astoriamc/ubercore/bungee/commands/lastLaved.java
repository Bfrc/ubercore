package net.astoriamc.ubercore.bungee.commands;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class lastLaved extends Command {

    private ResultSet result;
    private Statement statement;
    private BungeeCore BungeeCore;

    public lastLaved(BungeeCore bungeeCore) {
        super("lastLaved");
        this.BungeeCore = bungeeCore;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (player.hasPermission("UberCore.staff") || !(sender instanceof ProxiedPlayer) && args.length == 1) {
            Connection connection = BungeeCore.getConnection();
            Statement statement = null;
            try {
                statement = connection.createStatement();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
            int puuph = 0;
            try {
                String sql = "SELECT * FROM UUPH Where UUIDMCHASH = " + proxiedPlayer.getUniqueId().toString().hashCode() + ";";
                result = statement.executeQuery(sql);
                while (result.next()) {
                    puuph = result.getInt("UUPH");
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                result = statement.executeQuery("SELECT * FROM PlayerDataMc WHERE UUPH = " + puuph + " ;");
                String lastLeaved1 = null;
                while (result.next()) {
                    lastLeaved1 = result.getString("LEAVEDATE");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "The player last left: " + lastLeaved1));
                }
            } catch (SQLException a) {
                player.sendMessage("the player does not exist");
            }
        } else {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c dou don`t have permission to do that"));
        }
    }
}
