package net.astoriamc.ubercore.bungee.commands;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class lastJoined extends Command {

    private final BungeeCore plugin;
    private Statement statement;

    public lastJoined(BungeeCore plugin) {
        super("lastJoined");
        this.plugin = plugin;
        plugin.getProxy().getPluginManager().registerCommand(plugin, this);
    }

    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (player.hasPermission("UberCore.staff")) {
            try {
                this.statement = this.plugin.getConnection().createStatement();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
            long puuph = 0L;
            try {
                puuph = SqlStaticUtils.getUUPHbyMinecraftUUID(proxiedPlayer.getUniqueId(), this.plugin.getConnection());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                ResultSet result = this.statement.executeQuery("SELECT * FROM PlayerDataMc WHERE UUPH = " + puuph + " ;");
                while (result.next()) {
                    String lastLeaved1 = result.getString("LASTJOIN");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "The player joined: " + lastLeaved1));
                }
            } catch (SQLException ignored) {
            }
        } else {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c dou don`t have permission to do that"));
        }
    }
}
