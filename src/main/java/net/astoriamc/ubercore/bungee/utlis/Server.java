package net.astoriamc.ubercore.bungee.utlis;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

public class Server {
    private BungeeCore plugin;

    private ServerSocket serverSocket;

    public void start(int port, BungeeCore plugin) throws IOException {
        this.plugin = plugin;
        this.serverSocket = new ServerSocket(port);
        System.out.println("Socket server");
        while (true)
            (new EchoClientHandler(this.serverSocket.accept())).start();
    }

    public void stop() throws IOException {
        this.serverSocket.close();
    }

    private class EchoClientHandler extends Thread {
        private final Socket clientSocket;
        private String inputLine;
        private PrintWriter out;

        private BufferedReader in;

        public EchoClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        public void run() {
            ProxyServer proxy = ProxyServer.getInstance();
            try {
                this.out = new PrintWriter(this.clientSocket.getOutputStream(), true);
                this
                        .in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                String nameMc;
                long id;
                String name;
                ProxiedPlayer proxiedPlayer;
                TextComponent msg;
                String server;
                try {
                    if ((this.inputLine = this.in.readLine()) == null)
                        break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (this.inputLine.equals("ShutdownSession")) {
                    this.out.println(true);
                    break;
                }
                System.out.println(inputLine);
                JsonElement jsonElement = new JsonParser().parse(inputLine);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                this.out.println(true);
                switch (jsonObject.get("typeBungee").getAsString()) {
                    case "link":
                        nameMc = jsonObject.get("nameMc").getAsString();
                        id = jsonObject.get("id").getAsLong();
                        name = jsonObject.get("name").getAsString();
                        proxiedPlayer = ProxyServer.getInstance().getPlayer(nameMc);
                        msg = new TextComponent("§cClick Me");
                        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, (new ComponentBuilder("§aLink UWU")).create()));
                        msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/link " + id));

                        proxiedPlayer.sendMessage("The " + name + " wants to link his discord to your minecraft account if toy did not tried to link your account ignore this message else");
                        proxiedPlayer.sendMessage(msg);
                        continue;
                    case "forward":
                        out.println(true);
                        server = jsonObject.get("Server").getAsString();
                        System.out.println("yes");
                        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                            System.out.println(p.getName());
                            System.out.println(p.getServer().getInfo().getName());
                            if (p.getServer().getInfo().getName().equals(server.toLowerCase())) {
                                System.out.println(p.getName());
                                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                                out.writeUTF( inputLine );
                                p.getServer().getInfo().sendData( "as:ubercore", out.toByteArray() );
                                break;
                            }
                        }
                        this.out.println(false);
                        continue;
                    case "updateRang":
                        LuckPerms api = LuckPermsProvider.get();
                        int uuph = jsonObject.get("UUPH").getAsInt();
                        String rang = jsonObject.get("RANG").getAsString();
                        ProxiedPlayer player = null;
                        try {
                            player = ProxyServer.getInstance().getPlayer(SqlStaticUtils.getUUIDfromUUPH(uuph, plugin.getConnection()));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        api.getUserManager().getUser(player.getUniqueId()).setPrimaryGroup(rang);
                        continue;
                    case "ban":
                        uuph = jsonObject.get("UUPH").getAsInt();
                        String reson = jsonObject.get("REASON").getAsString();
                        player = null;
                        try {
                            player = ProxyServer.getInstance().getPlayer(SqlStaticUtils.getUUIDfromUUPH(uuph, plugin.getConnection()));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        proxy.getPluginManager().dispatchCommand(proxy.getConsole(), "ban "+player.getName()+" "+reson);
                    case "kick":
                        uuph = jsonObject.get("UUPH").getAsInt();
                        reson = jsonObject.get("REASON").getAsString();
                        player = null;
                        try {
                            player = ProxyServer.getInstance().getPlayer(SqlStaticUtils.getUUIDfromUUPH(uuph, plugin.getConnection()));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        proxy.getPluginManager().dispatchCommand(proxy.getConsole(), "kick "+player.getName()+" "+reson);
                    case "tempban":
                        uuph = jsonObject.get("UUPH").getAsInt();
                        reson = jsonObject.get("REASON").getAsString();
                        String time = jsonObject.get("DURATION").getAsString();
                        player = null;
                        try {
                            player = ProxyServer.getInstance().getPlayer(SqlStaticUtils.getUUIDfromUUPH(uuph, plugin.getConnection()));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        proxy.getPluginManager().dispatchCommand(proxy.getConsole(), "tempban "+player.getName()+" "+reson+" "+ time);
                }
            }
            try {
                this.in.close();
                this.out.close();
                this.clientSocket.close();
            } catch (Exception e) {
                System.out.println("error");
            }
        }
    }
}
