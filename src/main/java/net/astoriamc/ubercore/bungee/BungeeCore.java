package net.astoriamc.ubercore.bungee;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import net.astoriamc.ubercore.bungee.listeners.LuckPermsEvents;
import net.astoriamc.ubercore.bungee.listeners.onLpBan;
import net.astoriamc.ubercore.global.utils.mySqlInit;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

public class BungeeCore extends Plugin implements Listener {
    private final File configFile = new File(getDataFolder(), "config.yml");
    private Connection connection;

    public void onEnable() {
        saveDefultConfig();
        Configuration config = getConfig();
        mySqlInit initSql = new mySqlInit();
        consoleMessageSend("Getting info from config.");
        consoleMessageSend("MySql");
        consoleMessageSend(config.getInt("Port") + " " + config.getString("Database") + " " + config.getString("User") + " " + config.getString("Password"));
        try {
            this.connection = initSql.init(config.getString("Address"),
                    config.getInt("Port"),
                    config.getString("Database"),
                    config.getString("User"),
                    config.getString("Password"));
        } catch (SQLException throwables) {
            getLogger().info("mySql not working/has wrong credentials.");
        }
        getProxy().registerChannel("as:ubercore");
        getProxy().registerChannel("mod:astorioralis");

        Init init = new Init();
        init.essentialsInit(getConfig().getInt("socket-port"), this);

        init.Cinit(this);
        init.Linit(this);
        LuckPerms api = LuckPermsProvider.get();
        new LuckPermsEvents(this, api, connection);
        //onLpBan lp = new onLpBan();
        //lp.registerEvents();

        getLogger().info("╔═╦════════════════════╦═╗");
        getLogger().info("║D║   AstoriaUberCore  ║C║");
        getLogger().info("║U║ An exclusive plugin║O║");
        getLogger().info("║C║  for astoriamc.net ║R║");
        getLogger().info("║K╠════════════════════╣E║");
        getLogger().info("╠═╝    Made by Bfrc    ╚═╣");
        getLogger().info("╚════════════════════════╝");
    }

    public void consoleMessageSend(Object message) {
        getLogger().info(message.toString());
    }

    public Connection getConnection() {
        return this.connection;
    }

    public Configuration getConfig() {
        Configuration configuration = null;
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configuration;
    }

    public void saveConfig(Configuration configuration) throws IOException {
        ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(getDataFolder(), "config.yml"));
    }

    public void saveDefultConfig() {

        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
    }
}
