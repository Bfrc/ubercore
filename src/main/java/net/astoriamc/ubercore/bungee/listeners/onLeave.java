package net.astoriamc.ubercore.bungee.listeners;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class onLeave implements Listener {
    private final BungeeCore plugin;

    private Statement statement;

    public onLeave(BungeeCore plugin) {
        this.plugin = plugin;
        plugin.getProxy().getPluginManager().registerListener(plugin, this);
    }

    @EventHandler
    public void onPlayerDisconnectEvent(PlayerDisconnectEvent event) throws SQLException {
        Connection connection = plugin.getConnection();
        statement = connection.createStatement();
        long uuph = SqlStaticUtils.getUUPHbyMinecraftUUID(event.getPlayer().getUniqueId(), connection);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        String sql;
        sql = "UPDATE PlayerDataMc SET LEAVEDATE = \"" + date + "\"  WHERE UUPH = " + uuph + ";";
        statement.executeUpdate(sql);
    }
}
