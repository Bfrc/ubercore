package net.astoriamc.ubercore.bungee.listeners;


import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.event.EventBus;
import net.luckperms.api.event.user.track.UserDemoteEvent;
import net.luckperms.api.event.user.track.UserPromoteEvent;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class LuckPermsEvents {
    private final BungeeCore plugin;
    private final Connection connection;
    private Statement statement;
    public LuckPermsEvents(BungeeCore plugin, LuckPerms api, Connection connection) {
        this.connection = connection;
        this.plugin = plugin;
        statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        // get the LuckPerms event bus
        EventBus eventBus = api.getEventBus();

        // subscribe to an event using a method reference
        eventBus.subscribe(UserPromoteEvent.class, this::onUserPromote);
        eventBus.subscribe(UserDemoteEvent.class, this::onDemote);
        //TODO: Make messaging dc bot, test
    }

    private void onUserPromote(UserPromoteEvent event) {
        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> {
            String group = event.getUser().getPrimaryGroup();
            UUID uuid = event.getUser().getUniqueId();
            int uuph = 0;
            try {
                uuph = SqlStaticUtils.getUUPHbyMinecraftUUID(uuid,connection);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            String sql = "UPDATE UUPH SET RANG = \""+group+"\" WHERE UUPH = " + uuph + ";";
            try {
                statement.executeUpdate(sql);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        },0, TimeUnit.MINUTES);
    }
    private void onDemote(UserDemoteEvent event) {
        // as we want to access the Bukkit API, we need to use the scheduler to jump back onto the main thread.
        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> {
            String group = event.getUser().getPrimaryGroup();
            UUID uuid = event.getUser().getUniqueId();
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            int uuph = 0;
            try {
                uuph = SqlStaticUtils.getUUPHbyMinecraftUUID(uuid,connection);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            String sql = "UPDATE UUPH SET RANG = \""+group+"\" WHERE UUPH = \"" + uuph + "\";";
            try {
                statement.executeUpdate(sql);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            //TODO: Make messaging dc bot, test
        },0, TimeUnit.MINUTES);
    }

}