package net.astoriamc.ubercore.bungee.listeners;

import net.astoriamc.ubercore.bungee.BungeeCore;
import net.astoriamc.ubercore.global.utils.SqlStaticUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class onJoin implements Listener {
    private BungeeCore plugin;

    public onJoin(BungeeCore plugin) {
        this.plugin = plugin;
        plugin.getProxy().getPluginManager().registerListener(plugin, this);
    }


    @EventHandler
    public void onPlayerJoin(PostLoginEvent event) {
        LuckPerms api = LuckPermsProvider.get();
        String group = Objects.requireNonNull(api.getUserManager().getUser(event.getPlayer().getUniqueId())).getPrimaryGroup();
        ProxiedPlayer player = event.getPlayer();
        try {
            Connection connection = plugin.getConnection();
            Statement statement = connection.createStatement();
            int puuph = SqlStaticUtils.getUUPHbyMinecraftUUID(player.getUniqueId(), connection);
            if (puuph == -1) {
                long hash = SqlStaticUtils.generateUUPH(connection);
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
                Date date = new Date(System.currentTimeMillis());
                statement.execute("INSERT INTO UUPH(UUPH,UUIDMC,UUIDMCHASH,RANG)\n" +
                        "VALUES(" + hash + ",\"" + event.getPlayer().getUniqueId().toString() + "\"," + event.getPlayer().getUniqueId().toString().hashCode() + ",\""+group+"\");");
                statement.execute("INSERT INTO PlayerDataMc(UUPH,PlayerName,JOINDATE,LASTJOIN)\n" +
                        "VALUES(" + hash + ",\"" + event.getPlayer().getName() + "\",\"" + formatter.format(date) + "\",\"" + formatter.format(date) + "\");");
                statement.execute("INSERT INTO GlobalData(UUPH,UberCoins)\n" +
                        "VALUES(" + hash + ", 0);");
            } else {
                if (statement.executeQuery("SELECT EXISTS(SELECT * from PlayerDataMc WHERE UUPH = " + puuph + ");").equals(0)) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
                    Date date = new Date(System.currentTimeMillis());
                    statement.execute("INSERT INTO PlayerDataMc(UUPH,PlayerName,JOINDATE,LASTJOIN)\n" +
                            "VALUES(" + puuph + ",\"" + event.getPlayer().getName() + "\",\"" + formatter.format(date) + "\",\"" + formatter.format(date) + "\");");

                } else {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
                    Date date = new Date(System.currentTimeMillis());
                    String sql;
                    sql = "UPDATE PlayerDataMc SET LASTJOIN = \"" + formatter.format(date) + "\", PlayerName = \"" + event.getPlayer().getName() + "\"  WHERE UUPH = \"" + puuph + "\";";
                    statement.executeUpdate(sql);
                    sql = "UPDATE UUPH SET RANG = \""+group+"\" WHERE UUPH = \"" + puuph + "\";";
                    statement.executeUpdate(sql);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}